﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace ExportService.BusinessLayer
{
    public class PendhireServiceHelper
    {
        /// <summary>
        /// Post the Newhire data through Pendinghirs web api using sessionid
        /// </summary>
        /// <param name="jsonContract">JSON data</param>
        /// <param name="servicetype">Pending hires service type</param>
        /// <param name="sessionid">Session id fetched from UltiPro for a given username password</param>
        /// <param name="hostName">Host name where the service is being hosted</param>
        /// <param name="apikey">Got from Sevice page in UltiPro to get the client id</param>
        /// <param name="userapi">API key from super site</param>
        /// <param name="username">User Name</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public string PostUsingSessionId(string jsonContract, string servicetype, string sessionid, string hostName, string apikey, string userapi, string username, string password)
        {
            var content = new StringContent(jsonContract, Encoding.UTF8, "application/json");

            var httpclient = new HttpClient { BaseAddress = new UriBuilder("http", hostName).Uri };
            httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpclient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpclient.DefaultRequestHeaders.Add("US-Customer-Api-key", apikey);
            httpclient.DefaultRequestHeaders.Add("User-Api-key", userapi);
            if (string.IsNullOrEmpty(sessionid))
            {
                var basicAuthCredentials = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));
                httpclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", basicAuthCredentials);
            }
            else
            {
                httpclient.DefaultRequestHeaders.Add("US-SESSIONID", sessionid);
            }

            var response = httpclient.PostAsync(servicetype, content).Result;
            var statuscode = response.StatusCode.ToString();

            return (statuscode.ToUpper() != "ACCEPTED") || (statuscode.ToUpper() != "CREATED")
                ? statuscode
                : response.Content.ReadAsStringAsync().Result;
        }

        public static string Post(Models.NewHireDetails details)
        {
            if (details.ServiceType == Models.ServiceTypes.Onb14ToUltipro || details.ServiceType == Models.ServiceTypes.Rec14ToUltipro)
            {
                return PostNewGen(details.Ultidata,details.UserName, details.Password, details.ClientId,details.ServiceType, details.UseSuperSite, details.ClientAccessKey, details.HostName);
            }           
            return "FAILURE";
        }

        /// <summary>
        /// Post the Newhire data through Pendinghirs web api
        /// </summary>
        /// <param name="jsonContract">JSON Data</param>
        /// <param name="userName">Service User Name</param>
        /// <param name="password">Service Password</param>
        /// <param name="clientId">Client Id</param>
        /// <param name="servicetype">EndPoint type</param>
        /// <param name="UserSuperSite">Indicates whether the service go through super site</param>
        /// <param name="apikey">Api key from service management page</param>
        /// <param name="hostName">Where the Pendinghire Api hosted</param>
        /// <returns></returns>
        public static string PostNewGen(string jsonContract, string userName, string password, string clientId, Models.ServiceTypes srvtype, bool UseSuperSite, string apikey, string hostName = "localhost")
        {
            string servicetype = GetServiceType(srvtype);
            if (string.IsNullOrEmpty(servicetype))
            {
                return "Empty ServiceType received";
            }
            var httpclient = UseSuperSite ? new HttpClient { BaseAddress = new UriBuilder("http", hostName).Uri } : new HttpClient { BaseAddress = new UriBuilder("http", hostName, 9000).Uri };

            var content = new StringContent(jsonContract, Encoding.UTF8, "application/json");

            var basicAuthCredentials = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{userName}:{password}"));
            httpclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", basicAuthCredentials);

            httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            

            if (UseSuperSite)
            {
                httpclient.DefaultRequestHeaders.Add("Accept", "application/json");
                httpclient.DefaultRequestHeaders.Add("US-Customer-Api-key", apikey);
            }
            else
            {
                httpclient.DefaultRequestHeaders.Add("US-CLIENT-ID", clientId);
            }

            
            try
            {
                var response = httpclient.PostAsync(servicetype, content).Result;
                var statuscode = response.StatusCode.ToString();
                Debugger.Log(1, "Information", response.Content.ReadAsStringAsync().Result.ToString());
                var st = new StringBuilder();
                if ((statuscode.ToUpper() == "CREATED") || (statuscode.ToUpper() == "ACCEPTED"))
                {
                    return statuscode;
                }

                var msgcol = Resources.OdataHelper.GetErrorsFromODataErrorMessage(response);
                int cnt = 1;
                if (msgcol != null)
                    foreach (var err in msgcol)
                    {
                        st.AppendLine(cnt + "__" + err);
                        st.AppendLine();
                        cnt++;
                    }

                return st.ToString();
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
            
        }

      
        public static string GetServiceType(Models.ServiceTypes types)
        {
            var srvtype = string.Empty;
            switch (types)
            {
                case Models.ServiceTypes.Onb14ToUltipro:
                    srvtype = "services/employeehire/v1/onboardingpendinghires";
                    break;
                case Models.ServiceTypes.Rec14ToUltipro:
                case Models.ServiceTypes.Rec14ToOnb10:
                    srvtype = "services/employeehire/v1/recruitmentpendinghires";
                    break;
                case Models.ServiceTypes.EmployeeDocs:
                    srvtype = "services/employeeDocuments/v1/employeeDocuments";
                    break;
                case Models.ServiceTypes.Rec10ToUltipro:
                    srvtype = "WcfType";
                    break;
            }
            return srvtype;
        }
      
    }
}