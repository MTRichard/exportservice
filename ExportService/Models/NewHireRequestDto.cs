﻿namespace ExportService.Models
{
    public class NewHireRequestDto
    {
        public string Distination { get; set; }
        public string HostName { get; set; }
        public string ClientId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }
        public string InputJson { get; set; }
        public string RequestUser { get; set; }
    }
}
