﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ExportService.Models
{
    public class ExportDataModel
    {
        [BsonId]
        public ObjectId id { get; set; }
        public string ExportType { get; set; }
        public string ExportData { get; set; }
    }
}
