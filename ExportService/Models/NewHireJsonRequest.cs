﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ExportService.Models
{
    public class NewHireJsonRequest
    {
        [BsonId]
        public Object Id { get; set; }
        public CalingProducts CallingFrom { get; set; }
        public List<CallingCategories> CallingCategories { get; set; }
    }

    public enum CalingProducts
    {
        Onbording,
        Recruiting,
        ClassicOnbording
    }

    public enum CallingCategories
    {
        MinRecNewHire,
        MonOnb14NewHire,
        MinimalNewHire,
        MinCaNewhire,
        MinGlobalNewHire,
        NomalNewHire,
        FullNewHire,
        FullSupplemental,
        EmppersOnly,
        ContactOnly,
        EducationOnly,
        SkillsOnly,
        ExperienceOnly,
        FedIncome,
        StateIncome,
        ProvincialIncome,
        FedIncomeCa,
        ProvincialIncomeCa,
        CityIncomeCa,
        DirectDeposit,
        DicrectDepositCa
    }
}
