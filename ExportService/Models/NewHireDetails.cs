﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExportService.Models
{
    public class NewHireDetails
    {
        public string Url { get; set; }

        public string HostName { get; set; }

        public string Ultidata{ get; set; }

        public ServiceTypes ServiceType { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string SessionId { get; set; }

        public string ClientAccessKey { get; set; }

        public bool UseSuperSite { get; set; }

        public string ClientId { get; set; }

        public string OfferDetails { get; set; }

        public string SupplementalXml { get; set; }
    }

    public enum ServiceTypes
    {
        Onb10ToUltipro,
        Rec10ToUltipro,
        Rec10ToOnb10,
        Rec10ToOnb14,
        Rec14ToUltipro,
        Rec14ToOnb10,
        Onb14ToUltipro,
        ExternalApplication,
        EmployeeDocs
    }
}