﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace ExportService.Models
{
    public class NewHireJsonReturn
    {
        [BsonId]
        public Object MyProperty { get; set; }

        public List<NewHireProperty> NewHireRequestData { get; set; }
    }

    public class NewHireProperty
    {
        [BsonId]
        public ObjectId id { get; set; }

        public string ClientId { get; set; }
        public string Category { get; set; }

        public string NewHireJson { get; set; }
    }

    public class NewHireRequestDetails
    {
        [BsonId]
        public ObjectId id { get; set; }
        public DateTime InsertDateTime { get; set; }
        public string ClientId { get; set; }
        public string ServerName { get; set; }
        public string UserName { get; set; }
        public string EndPoint { get; set; }
        public string ReturnStatus { get; set; }     
    }
}
