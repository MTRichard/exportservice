﻿using Microsoft.Data.OData;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;

namespace ExportService.Resources
{
    public static class OdataHelper
    {
        public static ODataError GetODataError(HttpResponseMessage response)
        {           
            // Basic JSON deserialization, the ODataError deserializer is not public in the OData API:
            if (response.Content is StreamContent)
            {
                JObject json = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                JToken odataError = json["odata.error"];

                return new ODataError
                {
                    ErrorCode = odataError["code"].ToString(),
                    Message = odataError["message"]["value"].ToString().Trim(),
                    MessageLanguage = odataError["message"]["lang"].ToString(),
                    InnerError = odataError["innererror"] != null
                                          ? new ODataInnerError { Message = odataError["innererror"]["message"].ToString().Trim() }
                                          : new ODataInnerError()
                };
            }

            throw new InvalidOperationException();
        }

        public static string[] GetErrorsFromODataErrorMessage(HttpResponseMessage response)
        {
            ODataError errorResults = OdataHelper.GetODataError(response);

            return errorResults == null
                 ? null
                 : errorResults.InnerError.Message.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        }

    }
}
