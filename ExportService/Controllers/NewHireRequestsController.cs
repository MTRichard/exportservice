﻿using ExportService.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ExportService.Controllers
{
    [Produces("application/json")]
    [Route("api/NewHireRequests")]
    public class NewHireRequestsController : Controller
    {
        [HttpGet]
        [ProducesResponseType(typeof(List<NewHireRequestDetails>), 200)]
        public IActionResult Get()
        {
            var mongo = new ExportService.Repository.MongoRepository();
            var newhirelist = mongo.GetAllNewHireRequestDetails();
            return Ok(newhirelist);
        }
    }
}