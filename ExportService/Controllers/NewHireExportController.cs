﻿using ExportService.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using ExportService.Repository;

namespace ExportService.Controllers
{
    [Route("api/[controller]")]
    public class NewHireExportController : Controller
    {       
        [HttpPost] 
        public ActionResult Post([FromBody]NewHireRequestDto inputdata)
        {

            MongoRepository repository = new MongoRepository();
            if ( ValidateInputs(inputdata.InputJson, inputdata.ClientId, inputdata.HostName, inputdata.Username, inputdata.Password))
            {
               
                string statuscode = BusinessLayer.PendhireServiceHelper.PostNewGen(inputdata.InputJson, inputdata.Username, inputdata.Password, inputdata.ClientId, GetServiceTypeForExport(inputdata.Distination), !string.IsNullOrEmpty(inputdata.ApiKey), inputdata.ApiKey, inputdata.HostName);
                if ((statuscode.ToUpper() == "CREATED") || (statuscode.ToUpper() == "ACCEPTED"))
                {
                    NewHireRequestDetails details = new NewHireRequestDetails
                    {
                        InsertDateTime = DateTime.Now,
                        ClientId = inputdata.ClientId,
                        ServerName = inputdata.HostName,
                        EndPoint = inputdata.Distination,
                        ReturnStatus = statuscode.ToUpper(),
                        UserName = inputdata.RequestUser
                    };
                    repository.AddNewHireRequestItems(details);

                    return Ok(statuscode);
                }

                NewHireRequestDetails detailserror = new NewHireRequestDetails
                {
                    InsertDateTime = DateTime.Now,
                    ClientId = inputdata.ClientId,
                    ServerName = inputdata.HostName,
                    EndPoint = inputdata.Distination,
                    ReturnStatus = statuscode.ToUpper(),
                    UserName = inputdata.RequestUser
                };
                repository.AddNewHireRequestItems(detailserror);

                return BadRequest(statuscode);
            }
            NewHireRequestDetails validationerror = new NewHireRequestDetails
            {
                InsertDateTime = DateTime.Now,
                ClientId = inputdata.ClientId,
                ServerName = inputdata.HostName,
                EndPoint = inputdata.Distination,
                ReturnStatus = "Required fields validation failed",
                UserName = inputdata.RequestUser
            };
            repository.AddNewHireRequestItems(validationerror);

            return BadRequest("Required fields validation failed");
        }

        [Route("Recuriting")]
        [HttpPost]
        public ActionResult AddRecruitingNewHire([FromBody]string content, string clientid, string hostname, string username, string password, string apikey = "", bool usesupersite = false, string port = "")
        {
            if(ValidateInputs(content, clientid, hostname,username,password))
            {
                string statuscode = BusinessLayer.PendhireServiceHelper.PostNewGen(content, username, password, clientid, Models.ServiceTypes.Rec14ToUltipro, usesupersite, apikey, hostname);
                if ((statuscode.ToUpper() == "CREATED") || (statuscode.ToUpper() == "ACCEPTED"))
                {
                    return Ok(statuscode);
                }

                return BadRequest(statuscode);
            }
            return BadRequest("Required fields validation failed");

        }

        [Route("ClassicOnboarding")]
        [HttpPost]
        public string AddClassicOnboardingNewHire([FromBody]string content, string clientid, string hostname, string username, string password, string apikey = "", bool usesupersite = false, string port = "")
        {
            return string.Empty;
        }

        private bool ValidateInputs(string content, string clientid, string hostname, string username, string password)
        {
            if (string.IsNullOrEmpty(content) || string.IsNullOrWhiteSpace(content))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(clientid) || string.IsNullOrWhiteSpace(clientid))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(hostname) || string.IsNullOrWhiteSpace(hostname))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(username) || string.IsNullOrWhiteSpace(username))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password))
            {
                return false;
            }
            return true;
        }

        private ServiceTypes GetServiceTypeForExport(string product)
        {
            switch (product)
            {
                case "ONB14_ULTI_MIN":
                case "ONB14_ULTI_MAX":
                case "ONB14_ULTI_EMP_ONLY":
                case "ONB14_ULTI_WO_TALENT":
                    return ServiceTypes.Onb14ToUltipro;
                case "REC14_ULTI_MIN":
                case "REC14_ULTI_MAX":
                case "REC14_ULTI_WO_SUPP":
                case "REC14_ULTI_WO_TALENT":
                    return ServiceTypes.Rec14ToUltipro;
                case "REC14_ONB10":
                    return ServiceTypes.Rec14ToOnb10;
                case "ONB10_ULTI":
                    return ServiceTypes.Onb10ToUltipro;
                case "STYPE_ULTI":
                    return ServiceTypes.ExternalApplication;
                default:
                    return ServiceTypes.ExternalApplication;
            }
        }

        private NewHireRequestDto GetObjectFromJson(string request)
        {
            try
            {
                return JsonConvert.DeserializeObject<NewHireRequestDto>(request);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.Write(e.ToString());
                throw;
            }
        }
    }
}