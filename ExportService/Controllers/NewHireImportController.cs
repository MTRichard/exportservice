﻿using ExportService.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;


namespace ExportService.Controllers
{
    [Route("api/[controller]")]
    public class NewHireImportController : Controller
    {
        // GET: api/NewHireImport
        [HttpGet]
        [ProducesResponseType(typeof(List<NewHireProperty>), 200)]
        public IActionResult Get()
        {
            var mongo = new ExportService.Repository.MongoRepository();
            var hirelist = mongo.GetAllJson();
            return Ok(hirelist);
        }

        // GET: api/NewHireImport/Onboarding
        [HttpGet("{categories}", Name = "Get")]
        public string Get(List<CallingCategories> categories)
        {
            return "value";
        }      
    }
}
