﻿using ExportService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExportService.Repository
{
    public interface IMongoRepository
    {
        IEnumerable<NewHireJsonRequest> GetExportJson(CallingCategories categories);
        IEnumerable<NewHireProperty> GetAllJson();
        void AddItems(NewHireProperty prop);
    }
}
