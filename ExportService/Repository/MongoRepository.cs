﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExportService.Models;
using MongoDB.Driver;

namespace ExportService.Repository
{
    public class MongoRepository : IMongoRepository
    {

        private MongoClient _client { get; set; }
        private IMongoDatabase _database { get; set; }
        public MongoRepository()
        {       
            _client = new MongoClient();
            _database = _client.GetDatabase("ExportService");
        }
        public IEnumerable<NewHireProperty> GetAllJson()
        {
            var collec = _database.GetCollection<NewHireProperty>("NewHireRequest").AsQueryable().ToList();
            return collec;
        }

        public IEnumerable<NewHireJsonRequest> GetExportJson(CallingCategories categories)
        {
            throw new NotImplementedException();
        }

        public void AddItems(NewHireProperty prop)
        {
           var collection = _database.GetCollection <NewHireProperty> ("NewHireRequest");
            collection.InsertOne(prop);
        }

        public void AddNewHireRequestItems(NewHireRequestDetails prop)
        {
            var collection = _database.GetCollection<NewHireRequestDetails>("NewHireRequestDetails");
            collection.InsertOne(prop);
        }

        public IEnumerable<NewHireRequestDetails> GetAllNewHireRequestDetails()
        {
            var collec = _database.GetCollection<NewHireRequestDetails>("NewHireRequestDetails").AsQueryable().OrderByDescending(x => x.InsertDateTime).ToList();
            return collec;
        }
        void CreateCollection()
        {
            //#region Schema
            //string schema = @"{
            //                   validator: { $jsonSchema: {
            //                      bsonType: 'object',
            //                      required: [ 'category' ],
	           //                   required: [ 'NewHireJson' ],
            //                      properties: {
            //                         category: {
            //                            bsonType: 'string',
            //                            description: 'must be a string and is required'
            //                         },
            //                         NewHireJson: {
            //                            bsonType : 'string',
            //                            description: 'must be a string and match the regular expression pattern'
            //                         }
            //                      }
            //                   } }
            //                }";
            //#endregion
            _database.CreateCollection("NewHireJsonReturn");
        }
    }
}
