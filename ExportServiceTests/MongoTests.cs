using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ExportServiceTests
{
    [TestClass]
    public class MongoTests
    {
        ExportService.Repository.MongoRepository repo = new ExportService.Repository.MongoRepository();

        #region  Seed      
        [TestMethod]
        public void Onb14MinAddNewHireJsonTest()
        {
            ExportService.Models.NewHireProperty prop = new ExportService.Models.NewHireProperty();
            const string json = @"{
	                            ""CandidateKey"": ""9602778E-3782-4597-8B06-6B5237E5F2F3"",
	                            ""EmployeeHireData"": {
		                            ""NameFirst"": ""Onb"",
		                            ""NameLast"": ""Fourteen"",
		                            ""JobCode"": ""ONB"",
		                            ""Supervisor"": {
			                            ""Type"": ""EmployeeNumber"",
			                            ""Value"": ""596394023""
		                            }
	                            }
                            }";
            prop.Category = "ONB14_ULTI_MIN";
            prop.ClientId = "M0021";
            prop.id = new MongoDB.Bson.ObjectId();
            prop.NewHireJson = json;
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            mongo.AddItems(prop);                   
        }

        [TestMethod]
        public void Onb14FullEmpAddNewHireJsonTest()
        {
            ExportService.Models.NewHireProperty prop = new ExportService.Models.NewHireProperty();

            #region MyRegion
            const string json = @"{
	                                ""ImportedBy"" : ""TestImportedBy"",
	                                ""CandidateKey"" : ""72CC228E-4363-4667-8FD1-06DDFC298038"", 
		                                ""EmployeeHireData"" : {
			                                ""CompanyId"" : ""12345"",
			                                ""SsnOrSin"" : ""527527527"",
			                                ""NamePrefixCode"" : ""Mr"",
			                                ""NameFirst"" : ""RecUsa"",
			                                ""NameMiddle"" : ""To"",
			                                ""NameLast"" : ""NewGen"",
			                                ""NameSuffixCode"" : ""Jr"",
			                                ""EmployeeNumber"" : ""12345"",
			                                ""TimeclockId"" : ""12345"",
			                                ""AddressLine1"" : ""TestAddressLine1"",
			                                ""AddressLine2"" : ""TestAddressLine2"",
			                                ""AddressCountryCode"" : ""USA"",
			                                ""City"" : ""TestCity"",
			                                ""StateOrProvinceCode"" : ""ABCD"",
			                                ""ZipOrPostalCode"" : ""12345"",
			                                ""AddressCounty"" : ""12345"",
			                                ""HomePhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneExtension"" : ""1234"",
			                                ""OtherPhoneNumber"" : ""1234567890"",
			                                ""OtherPhoneTypeCode"" : ""CEL"",
			                                ""PrimaryEmailAddress"" : ""Primary@usg.com"",
			                                ""AlternateEmailAddress"" : ""Alternate@ultimatesoftware.com"",
			                                ""DateOfBirth"" : ""1999-03-12T15:00:53.1659578-04:00"",
			                                ""GenderCode"" : ""M"",
			                                ""MaritalStatusCode"" : ""M"",
			                                ""EthnicCode"" : ""1"",
			                                ""I9VerifiedCode"" : ""Y"",
			                                ""StartDate"" : ""2018-03-12T15:00:53.1659578-04:00"",
			                                ""JobGroupCode"" : ""ABCD"",
			                                ""JobCode"" : ""SERV"",
			                                ""JobTitle"" : null,
			                                ""LocationCode"" : ""ABCD"",
			                                ""HireSourceCode"" : ""ABCD"",
			                                ""OrgLevel1Code"" : ""NORTH"",
			                                ""OrgLevel2Code"" : ""HQ"",
			                                ""OrgLevel3Code"" : "" "",
			                                ""OrgLevel4Code"" : "" "",
			                                ""PayRate"" : ""10.0"",
			                                ""RatePerCode"" : ""H"",
			                                ""PayGroupCode"" : ""R"",
			                                ""ScheduledWorkHours"" : 10.0,
			                                ""PaymentsPerYear"" : 12,
			                                ""WeeklyHours"" : 80,
			                                ""EarningsGroupCode"" : ""40"",
			                                ""DeductionBenefitGroupCode"" : ""ABCD"",
			                                ""EmployeeTypeCode"" : ""R"",
			                                ""HourlyOrSalaryCode"" : ""H"",
			                                ""FullTimeOrPartTimeCode"" : ""F"",
			                                ""ShiftCode"" : ""AB"",
			                                ""PositionCode"" : null
		                                }

                                }
                                ";
            #endregion

            prop.Category = "ONB14_ULTI_EMP_ONLY";
            prop.ClientId = "M0021";
            prop.id = new MongoDB.Bson.ObjectId();
            prop.NewHireJson = json;
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            mongo.AddItems(prop);
        }

        [TestMethod]
        public void Onb14MaxAddNewHireJsonTest()
        {
            ExportService.Models.NewHireProperty prop = new ExportService.Models.NewHireProperty();

            #region Json

            const string json = @"{
	                                ""Id"" : 0,
	                                ""OfferId"" : ""TestOfferId"",
	                                ""RequisitionId"" : ""TestRequisitionId"",
	                                ""ImportedBy"" : ""TestImportedBy"",
	                                ""CandidateKey"" : ""72CC228E-4363-4667-8FD1-06DDFC298038"", 
		                                ""EmployeeHireData"" : {
			                                ""CompanyId"" : ""12345"",
			                                ""SsnOrSin"" : ""527527527"",
			                                ""NamePrefixCode"" : ""Mr"",
			                                ""NameFirst"" : ""OnbUsa"",
			                                ""NameMiddle"" : ""To"",
			                                ""NameLast"" : ""NewGen"",
			                                ""NameSuffixCode"" : ""Jr"",
			                                ""EmployeeNumber"" : ""12345"",
			                                ""TimeclockId"" : ""12345"",
			                                ""AddressLine1"" : ""TestAddressLine1"",
			                                ""AddressLine2"" : ""TestAddressLine2"",
			                                ""AddressCountryCode"" : ""USA"",
			                                ""City"" : ""TestCity"",
			                                ""StateOrProvinceCode"" : ""ABCD"",
			                                ""ZipOrPostalCode"" : ""12345"",
			                                ""AddressCounty"" : ""12345"",
			                                ""HomePhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneExtension"" : ""1234"",
			                                ""OtherPhoneNumber"" : ""1234567890"",
			                                ""OtherPhoneTypeCode"" : ""CEL"",
			                                ""PrimaryEmailAddress"" : ""Primary@usg.com"",
			                                ""AlternateEmailAddress"" : ""Alternate@ultimatesoftware.com"",
			                                ""DateOfBirth"" : ""1999-03-12T15:00:53.1659578-04:00"",
			                                ""GenderCode"" : ""M"",
			                                ""MaritalStatusCode"" : ""M"",
			                                ""EthnicCode"" : ""1"",
			                                ""I9VerifiedCode"" : ""Y"",
			                                ""StartDate"" : ""2018-03-12T15:00:53.1659578-04:00"",
			                                ""JobGroupCode"" : ""ABCD"",
			                                ""JobCode"" : ""SERV"",
			                                ""JobTitle"" : null,
			                                ""LocationCode"" : ""ABCD"",
			                                ""HireSourceCode"" : ""ABCD"",
			                                ""OrgLevel1Code"" : ""NORTH"",
			                                ""OrgLevel2Code"" : ""HQ"",
			                                ""OrgLevel3Code"" : "" "",
			                                ""OrgLevel4Code"" : "" "",
			                                ""PayRate"" : ""10.0"",
			                                ""RatePerCode"" : ""H"",
			                                ""PayGroupCode"" : ""R"",
			                                ""ScheduledWorkHours"" : 10.0,
			                                ""PaymentsPerYear"" : 12,
			                                ""WeeklyHours"" : 80,
			                                ""EarningsGroupCode"" : ""40"",
			                                ""DeductionBenefitGroupCode"" : ""ABCD"",
			                                ""EmployeeTypeCode"" : ""R"",
			                                ""HourlyOrSalaryCode"" : ""H"",
			                                ""FullTimeOrPartTimeCode"" : ""F"",
			                                ""ShiftCode"" : ""AB"",
			                                ""PositionCode"" : null
		                                },
		                                ""SupplementalData"" : {
			                                ""UsaFederalIncomeTax"" : {
				                                ""FilingStatusCode"" : ""S"",
				                                ""TotalAllowancesClaimed"" : 0,
				                                ""AdditionalAmountWithheld"" : ""0.00"",
				                                ""IsExemptFromWithholding"" : ""N""
			                                },
			                                ""UsaStateIncomeTax"" : {
				                                ""WorkInEmpTaxCalcOpt4"" : ""Y"",
				                                ""WorkInFilingStatusCode"" : ""S"",
				                                ""WorkInAllowancesClaimed"" : 0,
				                                ""WorkInAdditionalAmountWithheld"" : ""9.99"",
				                                ""WorkInIsExemptFromWithholding"" : ""n"",
				                                ""WorkInIsExemptFromTax"" : ""N"",
				                                ""WorkInAdditionalAllowances"" : 999,
				                                ""ResidentEmpTaxCalcOpt4"" : ""N"",
				                                ""ResidentFilingStatusCode"" : ""M"",
				                                ""ResidentAllowancesClaimed"" : 1,
				                                ""ResidentAdditionalAmountWithheld"" : ""0.00"",
				                                ""ResidentIsExemptFromWithholding"" : ""n"",
				                                ""ResidentIsExemptFromTax"" : ""n"",
				                                ""ResidentAdditionalAllowances"" : 0,
				                                ""ResidentCounty"" : ""BREWARD""
			                                },
			                                ""UserDefinedFieldsInfo"" : [{
					                                ""Document"" : ""I9"",
					                                ""FieldName"" : ""EntryField"",
					                                ""FieldExternalCode"" : ""SomeExternal Id"",
					                                ""Owner"" : ""Hiring Manager"",
					                                ""Value"" : ""Needed""
				                                }, {
					                                ""Owner"" : ""Admin""
				                                }
			                                ],
			                                ""UsaDirectDeposits"" : [{
					                                ""RoutingNumber"" : ""64646"",
					                                ""AccountTypeCode"" : ""S"",
					                                ""AccountNumber"" : ""5345345"",
					                                ""DepositRuleCode"" : ""A""
				                                }
			                                ],
			                                ""EmergencyContacts"" : [{
				                                ""NameFirst"" : ""SContact"",
				                                ""NameMiddle"" : ""S"",
				                                ""NameLast"" : ""SLast"",
				                                ""NameSuffixCode"" : ""Jr"",
				                                ""RelationshipCode"" : ""BRO"",
				                                ""CountryCode"" : ""USA"",
				                                ""AddressLine1"" : ""Saddress1"",
				                                ""AddressLine2"" : ""SAddress2"",
				                                ""City"" : ""Scity"",
				                                ""StateOrProvinceCode"" : ""FL"",
				                                ""ZipOrPostalCode"" : ""33326"",
				                                ""PreferredPhoneCode"" : ""H"",
				                                ""PhoneNumber"" : ""9543313000"",
				                                ""EmailAddress"" : ""SType@usg.com""
			                                }
		                                ],
                                            ""EducationInfo"": [
                                                  {
                                                    ""SchoolCode"": ""UF"",
                                                    ""FromDate"": ""2014-03-12T15:00:53.2959578-04:00"",
                                                    ""ToDate"": ""2014-03-14T15:00:53.2959578-04:00"",
                                                    ""Graduated"": true,
                                                    ""Years"": 1,
                                                    ""MajorCode"": ""ENG"",
                                                    ""MinorCode"": ""Bus"",
                                                    ""Location"": ""TestLocation"",
                                                    ""LevelCode"": ""B"",
                                                    ""Gpa"": ""4.0"",
                                                    ""Rank"": 1,
                                                    ""Notes"": ""TestNotes""
                                                  }
                                                ],
                                                ""ExperienceInfo"": [
                                                  {  
                                                    ""Employer"": ""TestEmployer"",
                                                    ""FromDate"": ""2014-03-12T15:00:53.3229578-04:00"",
                                                    ""ToDate"": ""2014-03-14T15:00:53.3229578-04:00"",
                                                    ""Position"": ""TestPosition"",
                                                    ""Location"": ""TestLocation"",
                                                    ""Salary"": ""10.0"",
                                                    ""SalaryPerCode"": ""M"",
                                                    ""ReasonForLeaving"": ""TestReasonForLeaving"",
                                                    ""Notes"": ""TestNotes""
                                                  }
                                                ],
                                                ""LicenseInfo"": [
                                                  {
                                                    ""LicenseCode"": ""PHR"",
                                                    ""LicenseNumber"": ""TestLicenseNumber"",
                                                    ""ReceivedDate"": ""2014-03-12T15:00:53.3389578-04:00"",
                                                    ""RenewalDate"": ""2014-03-14T15:00:53.3389578-04:00"",
                                                    ""ProviderCode"": ""USG""
                                                  }
                                                ],
                                                ""SkillInfo"": [
                                                  {
                                                    ""SkillCode"": ""EXCEL"",
                                                    ""ProficiencyCode"": ""EXPERT"",
                                                    ""AssesedOnDate"": ""2014-03-12T15:00:53.3489578-04:00"",
                                                    ""AssessedBy"": ""TestAssesedBy"",
                                                    ""Notes"": ""TestNotes""
                                                  }
                                                ]
		                                }

                                }";
            #endregion
            prop.Category = "ONB14_ULTI_MAX";
            prop.ClientId = "M0021";
            prop.id = new MongoDB.Bson.ObjectId();
            prop.NewHireJson = json;
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            mongo.AddItems(prop);
        }

        [TestMethod]
        public void Onb14MaxWithoutTalentAddNewHireJsonTest()
        {
            ExportService.Models.NewHireProperty prop = new ExportService.Models.NewHireProperty();
#region Json

            const string json = @"{
	                                ""Id"" : 0,
	                                ""OfferId"" : ""TestOfferId"",
	                                ""RequisitionId"" : ""TestRequisitionId"",
	                                ""ImportedBy"" : ""TestImportedBy"",
	                                ""CandidateKey"" : ""72CC228E-4363-4667-8FD1-06DDFC298038"", 
		                                ""EmployeeHireData"" : {
			                                ""CompanyId"" : ""12345"",
			                                ""SsnOrSin"" : ""527527527"",
			                                ""NamePrefixCode"" : ""Mr"",
			                                ""NameFirst"" : ""OnbUsa"",
			                                ""NameMiddle"" : ""To"",
			                                ""NameLast"" : ""NewGen"",
			                                ""NameSuffixCode"" : ""Jr"",
			                                ""EmployeeNumber"" : ""12345"",
			                                ""TimeclockId"" : ""12345"",
			                                ""AddressLine1"" : ""TestAddressLine1"",
			                                ""AddressLine2"" : ""TestAddressLine2"",
			                                ""AddressCountryCode"" : ""USA"",
			                                ""City"" : ""TestCity"",
			                                ""StateOrProvinceCode"" : ""ABCD"",
			                                ""ZipOrPostalCode"" : ""12345"",
			                                ""AddressCounty"" : ""12345"",
			                                ""HomePhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneExtension"" : ""1234"",
			                                ""OtherPhoneNumber"" : ""1234567890"",
			                                ""OtherPhoneTypeCode"" : ""CEL"",
			                                ""PrimaryEmailAddress"" : ""Primary@usg.com"",
			                                ""AlternateEmailAddress"" : ""Alternate@ultimatesoftware.com"",
			                                ""DateOfBirth"" : ""1999-03-12T15:00:53.1659578-04:00"",
			                                ""GenderCode"" : ""M"",
			                                ""MaritalStatusCode"" : ""M"",
			                                ""EthnicCode"" : ""1"",
			                                ""I9VerifiedCode"" : ""Y"",
			                                ""StartDate"" : ""2018-03-12T15:00:53.1659578-04:00"",
			                                ""JobGroupCode"" : ""ABCD"",
			                                ""JobCode"" : ""SERV"",
			                                ""JobTitle"" : null,
			                                ""LocationCode"" : ""ABCD"",
			                                ""HireSourceCode"" : ""ABCD"",
			                                ""OrgLevel1Code"" : ""NORTH"",
			                                ""OrgLevel2Code"" : ""HQ"",
			                                ""OrgLevel3Code"" : "" "",
			                                ""OrgLevel4Code"" : "" "",
			                                ""PayRate"" : ""10.0"",
			                                ""RatePerCode"" : ""H"",
			                                ""PayGroupCode"" : ""R"",
			                                ""ScheduledWorkHours"" : 10.0,
			                                ""PaymentsPerYear"" : 12,
			                                ""WeeklyHours"" : 80,
			                                ""EarningsGroupCode"" : ""40"",
			                                ""DeductionBenefitGroupCode"" : ""ABCD"",
			                                ""EmployeeTypeCode"" : ""R"",
			                                ""HourlyOrSalaryCode"" : ""H"",
			                                ""FullTimeOrPartTimeCode"" : ""F"",
			                                ""ShiftCode"" : ""AB"",
			                                ""PositionCode"" : null
		                                },
		                                ""SupplementalData"" : {
			                                ""UsaFederalIncomeTax"" : {
				                                ""FilingStatusCode"" : ""S"",
				                                ""TotalAllowancesClaimed"" : 0,
				                                ""AdditionalAmountWithheld"" : ""0.00"",
				                                ""IsExemptFromWithholding"" : ""N""
			                                },
			                                ""UsaStateIncomeTax"" : {
				                                ""WorkInEmpTaxCalcOpt4"" : ""Y"",
				                                ""WorkInFilingStatusCode"" : ""S"",
				                                ""WorkInAllowancesClaimed"" : 0,
				                                ""WorkInAdditionalAmountWithheld"" : ""9.99"",
				                                ""WorkInIsExemptFromWithholding"" : ""n"",
				                                ""WorkInIsExemptFromTax"" : ""N"",
				                                ""WorkInAdditionalAllowances"" : 999,
				                                ""ResidentEmpTaxCalcOpt4"" : ""N"",
				                                ""ResidentFilingStatusCode"" : ""M"",
				                                ""ResidentAllowancesClaimed"" : 1,
				                                ""ResidentAdditionalAmountWithheld"" : ""0.00"",
				                                ""ResidentIsExemptFromWithholding"" : ""n"",
				                                ""ResidentIsExemptFromTax"" : ""n"",
				                                ""ResidentAdditionalAllowances"" : 0,
				                                ""ResidentCounty"" : ""BREWARD""
			                                },
			                                ""UserDefinedFieldsInfo"" : [{
					                                ""Document"" : ""I9"",
					                                ""FieldName"" : ""EntryField"",
					                                ""FieldExternalCode"" : ""SomeExternal Id"",
					                                ""Owner"" : ""Hiring Manager"",
					                                ""Value"" : ""Needed""
				                                }, {
					                                ""Owner"" : ""Admin""
				                                }
			                                ],
			                                ""UsaDirectDeposits"" : [{
					                                ""RoutingNumber"" : ""64646"",
					                                ""AccountTypeCode"" : ""S"",
					                                ""AccountNumber"" : ""5345345"",
					                                ""DepositRuleCode"" : ""A""
				                                }
			                                ],
			                                ""EmergencyContacts"" : [{
				                                ""NameFirst"" : ""SContact"",
				                                ""NameMiddle"" : ""S"",
				                                ""NameLast"" : ""SLast"",
				                                ""NameSuffixCode"" : ""Jr"",
				                                ""RelationshipCode"" : ""BRO"",
				                                ""CountryCode"" : ""USA"",
				                                ""AddressLine1"" : ""Saddress1"",
				                                ""AddressLine2"" : ""SAddress2"",
				                                ""City"" : ""Scity"",
				                                ""StateOrProvinceCode"" : ""FL"",
				                                ""ZipOrPostalCode"" : ""33326"",
				                                ""PreferredPhoneCode"" : ""H"",
				                                ""PhoneNumber"" : ""9543313000"",
				                                ""EmailAddress"" : ""SType@usg.com""
			                                }
		                                ]
                                         
		                                }

                                }";
#endregion
            prop.Category = "ONB14_ULTI_WO_TALENT";
            prop.ClientId = "M0021";
            prop.id = new MongoDB.Bson.ObjectId();
            prop.NewHireJson = json;
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            mongo.AddItems(prop);
        }

        [TestMethod]
        public void Rec4MaxAddNewHireJsonTest()
        {
            ExportService.Models.NewHireProperty prop = new ExportService.Models.NewHireProperty();
            #region
            const string json = @"{
	                                ""Id"" : 0,
	                                ""OfferId"" : ""TestOfferId"",
	                                ""RequisitionId"" : ""TestRequisitionId"",
	                                ""ImportedBy"" : ""TestImportedBy"",
	                                ""CandidateKey"" : ""72CC228E-4363-4667-8FD1-06DDFC298038"", 
		                                ""EmployeeHireData"" : {
			                                ""CompanyId"" : ""12345"",
			                                ""SsnOrSin"" : ""527527527"",
			                                ""NamePrefixCode"" : ""Mr"",
			                                ""NameFirst"" : ""RecUsa"",
			                                ""NameMiddle"" : ""To"",
			                                ""NameLast"" : ""NewGen"",
			                                ""NameSuffixCode"" : ""Jr"",
			                                ""EmployeeNumber"" : ""12345"",
			                                ""TimeclockId"" : ""12345"",
			                                ""AddressLine1"" : ""TestAddressLine1"",
			                                ""AddressLine2"" : ""TestAddressLine2"",
			                                ""AddressCountryCode"" : ""USA"",
			                                ""City"" : ""TestCity"",
			                                ""StateOrProvinceCode"" : ""ABCD"",
			                                ""ZipOrPostalCode"" : ""12345"",
			                                ""AddressCounty"" : ""12345"",
			                                ""HomePhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneExtension"" : ""1234"",
			                                ""OtherPhoneNumber"" : ""1234567890"",
			                                ""OtherPhoneTypeCode"" : ""CEL"",
			                                ""PrimaryEmailAddress"" : ""Primary@usg.com"",
			                                ""AlternateEmailAddress"" : ""Alternate@ultimatesoftware.com"",
			                                ""DateOfBirth"" : ""1999-03-12T15:00:53.1659578-04:00"",
			                                ""GenderCode"" : ""M"",
			                                ""MaritalStatusCode"" : ""M"",
			                                ""EthnicCode"" : ""1"",
			                                ""I9VerifiedCode"" : ""Y"",
			                                ""StartDate"" : ""2018-03-12T15:00:53.1659578-04:00"",
			                                ""JobGroupCode"" : ""ABCD"",
			                                ""JobCode"" : ""SERV"",
			                                ""JobTitle"" : null,
			                                ""LocationCode"" : ""ABCD"",
			                                ""HireSourceCode"" : ""ABCD"",
			                                ""OrgLevel1Code"" : ""NORTH"",
			                                ""OrgLevel2Code"" : ""HQ"",
			                                ""OrgLevel3Code"" : "" "",
			                                ""OrgLevel4Code"" : "" "",
			                                ""PayRate"" : ""10.0"",
			                                ""RatePerCode"" : ""H"",
			                                ""PayGroupCode"" : ""R"",
			                                ""ScheduledWorkHours"" : 10.0,
			                                ""PaymentsPerYear"" : 12,
			                                ""WeeklyHours"" : 80,
			                                ""EarningsGroupCode"" : ""40"",
			                                ""DeductionBenefitGroupCode"" : ""ABCD"",
			                                ""EmployeeTypeCode"" : ""R"",
			                                ""HourlyOrSalaryCode"" : ""H"",
			                                ""FullTimeOrPartTimeCode"" : ""F"",
			                                ""ShiftCode"" : ""AB"",
			                                ""PositionCode"" : null
		                                },
		                                ""SupplementalData"" : {
			                                 ""EducationInfo"": [
                                              {
                                                ""SchoolCode"": ""UF"",
                                                ""FromDate"": ""2014-03-12T15:00:53.2959578-04:00"",
                                                ""ToDate"": ""2014-03-14T15:00:53.2959578-04:00"",
                                                ""Graduated"": true,
                                                ""Years"": 1,
                                                ""MajorCode"": ""ENG"",
                                                ""MinorCode"": ""Bus"",
                                                ""Location"": ""TestLocation"",
                                                ""LevelCode"": ""B"",
                                                ""Gpa"": ""4.0"",
                                                ""Rank"": 1,
                                                ""Notes"": ""TestNotes""
                                              }
                                            ],
                                            ""ExperienceInfo"": [
                                              {  
                                                ""Employer"": ""TestEmployer"",
                                                ""FromDate"": ""2014-03-12T15:00:53.3229578-04:00"",
                                                ""ToDate"": ""2014-03-14T15:00:53.3229578-04:00"",
                                                ""Position"": ""TestPosition"",
                                                ""Location"": ""TestLocation"",
                                                ""Salary"": ""10.0"",
                                                ""SalaryPerCode"": ""M"",
                                                ""ReasonForLeaving"": ""TestReasonForLeaving"",
                                                ""Notes"": ""TestNotes""
                                              }
                                            ],
                                            ""LicenseInfo"": [
                                              {
                                                ""LicenseCode"": ""PHR"",
                                                ""LicenseNumber"": ""TestLicenseNumber"",
                                                ""ReceivedDate"": ""2014-03-12T15:00:53.3389578-04:00"",
                                                ""RenewalDate"": ""2014-03-14T15:00:53.3389578-04:00"",
                                                ""ProviderCode"": ""USG""
                                              }
                                            ],
                                            ""SkillInfo"": [
                                              {
                                                ""SkillCode"": ""EXCEL"",
                                                ""ProficiencyCode"": ""EXPERT"",
                                                ""AssesedOnDate"": ""2014-03-12T15:00:53.3489578-04:00"",
                                                ""AssessedBy"": ""TestAssesedBy"",
                                                ""Notes"": ""TestNotes""
                                              }
                                            ]
		                                }

                                }";
#endregion
            prop.Category = "REC14_ULTI_MAX";
            prop.ClientId = "M0021";
            prop.id = new MongoDB.Bson.ObjectId();
            prop.NewHireJson = json;
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            mongo.AddItems(prop);
        }

        [TestMethod]
        public void Rec4MaxWithoutSuppAddNewHireJsonTest()
        {
            ExportService.Models.NewHireProperty prop = new ExportService.Models.NewHireProperty();

            #region  Json
            const string json = @"{
	                                ""Id"" : 0,
	                                ""OfferId"" : ""TestOfferId"",
	                                ""RequisitionId"" : ""TestRequisitionId"",
	                                ""ImportedBy"" : ""TestImportedBy"",
	                                ""CandidateKey"" : ""72CC228E-4363-4667-8FD1-06DDFC298038"", 
		                                ""EmployeeHireData"" : {
			                                ""CompanyId"" : ""12345"",
			                                ""SsnOrSin"" : ""527527527"",
			                                ""NamePrefixCode"" : ""Mr"",
			                                ""NameFirst"" : ""RecUsaFull"",
			                                ""NameMiddle"" : ""To"",
			                                ""NameLast"" : ""NewGen"",
			                                ""NameSuffixCode"" : ""Jr"",
			                                ""EmployeeNumber"" : ""12345"",
			                                ""TimeclockId"" : ""12345"",
			                                ""AddressLine1"" : ""TestAddressLine1"",
			                                ""AddressLine2"" : ""TestAddressLine2"",
			                                ""AddressCountryCode"" : ""USA"",
			                                ""City"" : ""TestCity"",
			                                ""StateOrProvinceCode"" : ""ABCD"",
			                                ""ZipOrPostalCode"" : ""12345"",
			                                ""AddressCounty"" : ""12345"",
			                                ""HomePhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneNumber"" : ""1234567890"",
			                                ""BusinessPhoneExtension"" : ""1234"",
			                                ""OtherPhoneNumber"" : ""1234567890"",
			                                ""OtherPhoneTypeCode"" : ""CEL"",
			                                ""PrimaryEmailAddress"" : ""Primary@usg.com"",
			                                ""AlternateEmailAddress"" : ""Alternate@ultimatesoftware.com"",
			                                ""DateOfBirth"" : ""1999-03-12T15:00:53.1659578-04:00"",
			                                ""GenderCode"" : ""M"",
			                                ""MaritalStatusCode"" : ""M"",
			                                ""EthnicCode"" : ""1"",
			                                ""I9VerifiedCode"" : ""Y"",
			                                ""StartDate"" : ""2018-03-12T15:00:53.1659578-04:00"",
			                                ""JobGroupCode"" : ""ABCD"",
			                                ""JobCode"" : ""SERV"",
			                                ""JobTitle"" : null,
			                                ""LocationCode"" : ""ABCD"",
			                                ""HireSourceCode"" : ""ABCD"",
			                                ""OrgLevel1Code"" : ""NORTH"",
			                                ""OrgLevel2Code"" : ""HQ"",
			                                ""OrgLevel3Code"" : "" "",
			                                ""OrgLevel4Code"" : "" "",
			                                ""PayRate"" : ""10.0"",
			                                ""RatePerCode"" : ""H"",
			                                ""PayGroupCode"" : ""R"",
			                                ""ScheduledWorkHours"" : 10.0,
			                                ""PaymentsPerYear"" : 12,
			                                ""WeeklyHours"" : 80,
			                                ""EarningsGroupCode"" : ""40"",
			                                ""DeductionBenefitGroupCode"" : ""ABCD"",
			                                ""EmployeeTypeCode"" : ""R"",
			                                ""HourlyOrSalaryCode"" : ""H"",
			                                ""FullTimeOrPartTimeCode"" : ""F"",
			                                ""ShiftCode"" : ""AB"",
			                                ""PositionCode"" : null
		                                }

                                }
                                ";
            #endregion
            prop.Category = "REC14_ULTI_WO_SUPP";
            prop.ClientId = "M0021";
            prop.id = new MongoDB.Bson.ObjectId();
            prop.NewHireJson = json;
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            mongo.AddItems(prop);
        }

        [TestMethod]
        public void Rec14MinAddNewHireJsonTest()
        {
            ExportService.Models.NewHireProperty prop = new ExportService.Models.NewHireProperty();
            #region json
            const string json = @"{
	                            ""CandidateKey"": ""9602778E-3782-4597-8B06-6B5237E5F2F3"",
	                            ""EmployeeHireData"": {
		                            ""NameFirst"": ""Rec"",
		                            ""NameLast"": ""Fourteen"",
		                            ""JobCode"": ""REC"",
		                            ""Supervisor"": {
			                            ""Type"": ""EmployeeNumber"",
			                            ""Value"": ""596394023""
		                            }
	                            }
                            }";
#endregion
            prop.Category = "REC14_ULTI_MIN";
            prop.ClientId = "M0021";
            prop.id = new MongoDB.Bson.ObjectId();
            prop.NewHireJson = json;
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            mongo.AddItems(prop);
        }

        #endregion
        [TestMethod]
        public void CheckTheCollectionInTheRepo()
        {
            ExportService.Repository.MongoRepository mongo = new ExportService.Repository.MongoRepository();
            IEnumerable<ExportService.Models.NewHireProperty> res = mongo.GetAllJson();
            Assert.AreEqual(1, 1);
        }
    }
}
