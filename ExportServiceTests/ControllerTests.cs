﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using ExportService.Controllers;
using ExportService.Models;
using Newtonsoft.Json;

namespace ExportServiceTests
{
    [TestClass]
    public class ControllerTests
    {
        const string json = @"{
	""CandidateKey"": ""9BCAE905-FE97-497E-A9FE-3025F21FD4FC"",
	""EmployeeHireData"": {
		""NameFirst"": ""DwyaneEU"",
		""NameLast"": ""DwyaneEU"",
		""JobCode"": ""Dwyane"",
		""Supervisor"": {
			""Type"": ""EmployeeNumber"",
			""Value"": ""596394023""
		}
	}
}";
        [TestMethod]
        public void CheckTheGetReturnData()
        {
            var controller = new ExportService.Controllers.NewHireImportController();
            var ret = controller.Get();
            Assert.AreEqual(1, 1);
        }

       

        [TestMethod]
        public void CallApiExportEndpoint()
        {
            HttpClient httpclient;
            var inputdata = new NewHireRequestDto
            {
                HostName = "MTR2M2app",
                ApiKey = string.Empty,
                ClientId = "M0021",
                Distination = "REC14_ULTI",
                InputJson = json,
                Username = "WsfAllAccessM0021",
                Password = "password"
            };
            string jsonrequest = JsonConvert.SerializeObject(inputdata);
            httpclient = new HttpClient { BaseAddress = new UriBuilder("http", "localhost", 45307).Uri };

            var content = new StringContent(jsonrequest, Encoding.UTF8, "application/json");
            httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            try
            {
                var response = httpclient.PostAsync("/api/NewHireExport", content).Result;
                var statuscode = response.StatusCode.ToString();
                Assert.AreEqual("OK", statuscode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
